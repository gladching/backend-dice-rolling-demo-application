## README
#### Author: Gladys Ching

### What is this repository for? ###

* Demo application for Dice Rolling Simulator

### Setting-up ###

* Dependencies
    - This demo uses Java 8.0 and Maven build in order to run
* Database configuration
    - H2 database is used to have a ready in memory database for the application.
      This means that the data would be created on the build of the application.
    - Database Entities:
        1. Simulation Details contains the dice count, dice sides, roll count
        2. Roll Results contains the sum result per role and the corresponding simulation id

### The project includes three endpoints: ###
* POST /dice-simulator?diceCount=8&diceSides=4&rollCount=3
    - Creates a record of the simulation details and roll results
    - Validation of the request parameters (dice count and roll count must be between 1 and 500;
      dice sides must be between 4 and 100)
    - If the caller did not provide the query parameters, the system will pick up default values
      (i.e. 3 for dice count, 6 for dice sides and 100 for roll count)
* GET /dice-simulator/roll-details
    - Retrieve the records of all the roll details including dice count, dice sides, total rolls and simulations count
* GET /dice-simulator/distribution-details?diceCount=8&diceSides=4
    - Retrieve the records of all the distribution details such as dice count, dice sides, result sum, total rolls and distribution percentage in % string (e.g. 33%, 1.5%)
