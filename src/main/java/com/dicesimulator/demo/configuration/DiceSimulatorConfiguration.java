package com.dicesimulator.demo.configuration;

import com.dicesimulator.demo.core.DefaultDiceSimulatorService;
import com.dicesimulator.demo.core.DiceSimulatorRepositoy;
import com.dicesimulator.demo.core.DiceSimulatorService;
import com.dicesimulator.demo.infrastructure.DefaultDiceSimulatorRepository;
import com.dicesimulator.demo.infrastructure.RollResultJpaRepository;
import com.dicesimulator.demo.infrastructure.SimulatorJpaRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DiceSimulatorConfiguration {

    @Bean
    DiceSimulatorRepositoy diceSimulatorRepository(RollResultJpaRepository rollResultJpaRepository,
                                                  SimulatorJpaRepository simulatorJpaRepository) {
        return new DefaultDiceSimulatorRepository(rollResultJpaRepository, simulatorJpaRepository);
    }

    @Bean
    DiceSimulatorService diceSimulatorService(DiceSimulatorRepositoy diceSimulatorRepositoy) {
        return new DefaultDiceSimulatorService(diceSimulatorRepositoy);
    }
}
