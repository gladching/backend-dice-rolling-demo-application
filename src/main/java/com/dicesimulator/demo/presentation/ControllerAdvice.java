package com.dicesimulator.demo.presentation;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ControllerAdvice {
    private final String INVALID_REQUEST_ERROR = "INVALID_REQUEST";
    private final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";

            @ResponseStatus(value = HttpStatus.BAD_REQUEST)
            @ExceptionHandler(value
                = { ConstraintViolationException.class })
        protected ResponseEntity<Object> handleConflict(
                    ConstraintViolationException  ex, WebRequest request) {

            List<String> errors = new ArrayList<String>();
            for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
                errors.add(violation.getPropertyPath() + ": " + violation.getMessage());
            }

            ExceptionResponse exceptionResponse = new ExceptionResponse();
            exceptionResponse.setCode(INVALID_REQUEST_ERROR);
            exceptionResponse.setMessage("Invalid parameters given");
            exceptionResponse.setInternalErrors(errors);

            return new ResponseEntity<Object>(exceptionResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        @ExceptionHandler(value = {RestClientException.class})
        protected ResponseEntity<Object> handleRestClientException(
                RestClientException ex, WebRequest request) {

                    ExceptionResponse exceptionResponse = new ExceptionResponse();
                    exceptionResponse.setCode(INTERNAL_SERVER_ERROR);
                    exceptionResponse.setMessage("Internal Server Error");

                    return new ResponseEntity<Object>(exceptionResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
                }

}
