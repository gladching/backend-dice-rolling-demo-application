package com.dicesimulator.demo.presentation;

import com.dicesimulator.demo.core.DiceSimulatorService;
import com.dicesimulator.demo.core.dto.DiceSimulatorRequestDTO;
import com.dicesimulator.demo.core.dto.DiceSimulatorResponseDTO;
import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/dice-simulator")
@Validated
public class DiceSimulatorController {
    @Autowired
    private DiceSimulatorService diceSimulatorService;

    @PostMapping
    public ResponseEntity<List<DiceSimulatorResponseDTO>> simulateDiceRoll(
            @Validated DiceSimulatorRequestDTO requestDTO) {
        List<DiceSimulatorResponseDTO> rollResultEntities;
        try {
             rollResultEntities = diceSimulatorService.simulateDiceRoll(requestDTO);
        } catch (Exception e) {
            throw new RestClientException("Saving of data failed");
        }
        return new ResponseEntity(rollResultEntities, HttpStatus.CREATED);
    }

    @GetMapping(value = "/roll-details", produces = "application/json")
    public ResponseEntity<List<SimulationResultsDTO>> getSimulationTotalDetails() {
        List<SimulationResultsDTO> simulationDetails = diceSimulatorService.getSimulationDetails();
        return new ResponseEntity(simulationDetails, HttpStatus.OK);

    }

    @GetMapping(value = "/distribution-details", produces = "application/json")
    public ResponseEntity<List<DistributionPercentageResultsDTO>> getDistributionPercentages(
            @RequestParam(defaultValue = "3") @Min(1) @Max(500) int diceCount,
            @RequestParam(defaultValue = "6") @Min(4) @Max(500) int diceSides
    ) {
        List<DistributionPercentageResultsDTO> distributionPercentageResults
                = diceSimulatorService.getDistributionPercentages(diceCount, diceSides);

        return new ResponseEntity(distributionPercentageResults, HttpStatus.OK);
    }
}
