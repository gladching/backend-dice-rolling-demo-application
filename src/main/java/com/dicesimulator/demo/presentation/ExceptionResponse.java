package com.dicesimulator.demo.presentation;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExceptionResponse {
    private String code;
    private String message;
    private Object internalErrors;
}
