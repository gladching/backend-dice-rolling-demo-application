package com.dicesimulator.demo.infrastructure;

import com.dicesimulator.demo.infrastructure.entity.RollResultEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RollResultJpaRepository extends JpaRepository<RollResultEntity, Integer> {
}
