package com.dicesimulator.demo.infrastructure;

import com.dicesimulator.demo.core.DiceSimulatorRepositoy;
import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;
import com.dicesimulator.demo.infrastructure.entity.SimulationEntity;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultDiceSimulatorRepository implements DiceSimulatorRepositoy {

    private final RollResultJpaRepository rollResultJpaRepository;
    private final SimulatorJpaRepository simulatorJpaRepository;

    public DefaultDiceSimulatorRepository(RollResultJpaRepository rollResultJpaRepository, SimulatorJpaRepository simulatorJpaRepository) {
        this.rollResultJpaRepository = rollResultJpaRepository;
        this.simulatorJpaRepository = simulatorJpaRepository;
    }

    @Override
    public void save(SimulationEntity simulationEntity) {
        simulatorJpaRepository.save(simulationEntity);
    }

    @Override
    public List<SimulationResultsDTO> getSimulationDetails() {
         List<?> simulationEntities = simulatorJpaRepository.getSimulatorDetailsGroupByDiceCountAndDiceSides();

         List<SimulationResultsDTO> simulationResults =
                 simulationEntities.stream()
                    .map(e -> buildResult((HashMap) e))
                    .collect(Collectors.toList());

         System.out.println(simulationResults);

         return simulationResults;
    }

    @Override
    public List<DistributionPercentageResultsDTO> getDistributionPercentages(int diceCount, int diceSides) {
        List<?> distributionPercentages = simulatorJpaRepository.getAllSimulationTotalRollDistribution(diceCount, diceSides);
        List<DistributionPercentageResultsDTO> results =
                distributionPercentages.stream()
                    .map(p -> buildDistributionPercentage((HashMap) p))
                    .collect(Collectors.toList());
        return results;
    }

    private DistributionPercentageResultsDTO buildDistributionPercentage(HashMap entity) {
      return DistributionPercentageResultsDTO.builder()
              .diceCount((int)entity.get("diceCount"))
              .diceSides((int)entity.get("diceSides"))
              .resultSum((int)entity.get("resultSum"))
              .totalRolls((long)entity.get("totalRolls"))
              .distributionPercentage(toPercentDisplay((double)entity.get("distributionPercentage")))
              .build();
    }

    private String toPercentDisplay(Double distributionInDecimal) {
        return String.format("%.2f%s", distributionInDecimal*100, "%");
    }

    private SimulationResultsDTO buildResult(HashMap entity) {
        SimulationResultsDTO simulationResults = new SimulationResultsDTO();
        simulationResults.setDiceCount((int)entity.get("diceCount"));
        simulationResults.setDiceSides((int)entity.get("diceSides"));
        simulationResults.setTotalRolls((long)entity.get("totalRolls"));
        simulationResults.setSimulationsCount((long) entity.get("simulationsCount"));
        return simulationResults;
    }
}
