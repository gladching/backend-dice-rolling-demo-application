package com.dicesimulator.demo.infrastructure.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "simulation_details")
@Getter
@Setter
@AllArgsConstructor
public class SimulationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "dice_count")
    private int diceCount;

    @Column(name = "dice_sides")
    private int diceSides;

    @Column(name = "roll_count")
    private int rollCount;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "simulationEntity", cascade = CascadeType.ALL)
    private List<RollResultEntity> rollResultEntity;

    public SimulationEntity(int diceCount, int diceSides, int rollCount) {
        this.diceCount = diceCount;
        this.diceSides = diceSides;
        this.rollCount = rollCount;
    }
}
