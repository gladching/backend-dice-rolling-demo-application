package com.dicesimulator.demo.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "roll_results")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RollResultEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "result_sum")
    private int resultSum;

    @ManyToOne
    @JoinColumn(name="simulator_id")
    private SimulationEntity simulationEntity;
}
