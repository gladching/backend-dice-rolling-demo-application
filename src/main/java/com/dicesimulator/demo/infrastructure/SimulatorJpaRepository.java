package com.dicesimulator.demo.infrastructure;

import com.dicesimulator.demo.infrastructure.entity.SimulationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface SimulatorJpaRepository extends JpaRepository<SimulationEntity, Integer> {
    @Query(value = "select new map(s.diceCount as diceCount, s.diceSides as diceSides, sum(s.rollCount) as totalRolls, count(s.id) as simulationsCount)" +
                        "from SimulationEntity s group by s.diceCount, s.diceSides")
    List<?> getSimulatorDetailsGroupByDiceCountAndDiceSides();


    @Query(value = "select new map(s.diceCount as diceCount, s.diceSides as diceSides, " +
            "count(s.id) as totalRolls, r.resultSum as resultSum, " +
            "count(s.id)*1.0/(select sum(s.rollCount) from SimulationEntity s where s.diceCount = ?1 and s.diceSides = ?2) as distributionPercentage) " +
            "from SimulationEntity s inner join RollResultEntity r on s.id = r.simulationEntity.id where s.diceCount = ?1 and s.diceSides = ?2 " +
            "group by s.diceCount, s.diceSides, r.resultSum " )
    List<?> getAllSimulationTotalRollDistribution(int diceCount, int diceSides);
}
