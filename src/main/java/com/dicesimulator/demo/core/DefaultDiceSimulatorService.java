package com.dicesimulator.demo.core;

import com.dicesimulator.demo.core.dto.DiceSimulatorRequestDTO;
import com.dicesimulator.demo.core.dto.DiceSimulatorResponseDTO;
import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.core.exception.RepositoryUpdateException;
import com.dicesimulator.demo.infrastructure.entity.RollResultEntity;
import com.dicesimulator.demo.infrastructure.entity.SimulationEntity;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class DefaultDiceSimulatorService implements DiceSimulatorService {

    private final DiceSimulatorRepositoy diceSimulatorRepositoy;

    public DefaultDiceSimulatorService(DiceSimulatorRepositoy diceSimulatorRepositoy) {
        this.diceSimulatorRepositoy = diceSimulatorRepositoy;
    }

    public List<DiceSimulatorResponseDTO> simulateDiceRoll(DiceSimulatorRequestDTO requestDTO) {
        int diceCount = requestDTO.getDiceCount();
        int diceSides = requestDTO.getDiceSides();
        int diceRolls = requestDTO.getRollCount();

        SimulationEntity simulationEntity = new SimulationEntity(diceCount, diceSides, diceRolls);

        List<RollResultEntity> rollResultEntities = getRollResultEntities(diceCount, diceSides, diceRolls, simulationEntity);

        simulationEntity.setRollResultEntity(rollResultEntities);
        try {
            diceSimulatorRepositoy.save(simulationEntity);
        } catch (Exception e) {
            throw new RepositoryUpdateException("Unable to save simulations in the database");
        }

        return getDistribution(rollResultEntities);

    }


    @Override
    public List<SimulationResultsDTO> getSimulationDetails() {
        return diceSimulatorRepositoy.getSimulationDetails();
    }

    @Override
    public List<DistributionPercentageResultsDTO> getDistributionPercentages(int diceCount, int diceSides) {
        return diceSimulatorRepositoy.getDistributionPercentages(diceCount, diceSides);
    }

    private List<DiceSimulatorResponseDTO> getDistribution(List<RollResultEntity> rollResults) {
        List<DiceSimulatorResponseDTO> results =
                rollResults.stream()
                        .collect(Collectors.groupingBy(RollResultEntity::getResultSum,
                                Collectors.counting()))
                        .entrySet().stream()
                        .map(m -> new DiceSimulatorResponseDTO((int)m.getKey(),(long)m.getValue()))
                        .collect(Collectors.toList());

        return results;
    }

    private List<RollResultEntity> getRollResultEntities(int diceCount, int diceSides,
                                                         int diceRolls, SimulationEntity simulationEntity) {

        List<RollResultEntity> rollResultEntities = new ArrayList<>();

        for (int i = 0; i < diceRolls; i++) {
            int rollTotal = 0;
            for (int j = 0; j < diceCount; j++) {
                Random r = new Random();
                rollTotal += r.nextInt((diceSides - 1) + 1) + 1;
            }

            RollResultEntity rollResultEntity = new RollResultEntity();
            rollResultEntity.setResultSum(rollTotal);
            rollResultEntity.setSimulationEntity(simulationEntity);
            rollResultEntities.add(rollResultEntity);
        }
        return rollResultEntities;
    }
}
