package com.dicesimulator.demo.core.exception;

public class RepositoryUpdateException extends RuntimeException {
    public RepositoryUpdateException(String errorMessage) {
        super(errorMessage);
    }
}
