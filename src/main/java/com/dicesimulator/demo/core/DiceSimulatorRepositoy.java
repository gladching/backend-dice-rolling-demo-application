package com.dicesimulator.demo.core;

import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.infrastructure.entity.SimulationEntity;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;

import java.util.List;

public interface DiceSimulatorRepositoy {
    void save(SimulationEntity simulationEntity);
    List<SimulationResultsDTO> getSimulationDetails();
    List<DistributionPercentageResultsDTO> getDistributionPercentages(int diceCount, int diceSides);
}
