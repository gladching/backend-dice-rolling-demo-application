package com.dicesimulator.demo.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Setter
@Getter
@AllArgsConstructor
public class DiceSimulatorRequestDTO {
    @Min(1)
    @Max(500)
    int diceCount = 3;      // set default value

    @Min(4)
    @Max(100)
    int diceSides = 6;

    @Min(1)
    @Max(500)
    int rollCount = 100;
}
