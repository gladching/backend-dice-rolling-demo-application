package com.dicesimulator.demo.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DiceSimulatorResponseDTO {
    private int resultSum;
    private long numberOfTimesRolled;
}
