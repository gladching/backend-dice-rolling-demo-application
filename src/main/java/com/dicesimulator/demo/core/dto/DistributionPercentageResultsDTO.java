package com.dicesimulator.demo.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DistributionPercentageResultsDTO {
    private int diceCount;
    private int diceSides;
    private int resultSum;
    private long totalRolls;
    private String distributionPercentage;
}
