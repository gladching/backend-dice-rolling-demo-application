package com.dicesimulator.demo.core.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimulationResultsDTO {
    private int diceCount;
    private int diceSides;
    private long totalRolls;
    private long simulationsCount;
}
