package com.dicesimulator.demo.core;

import com.dicesimulator.demo.core.dto.DiceSimulatorRequestDTO;
import com.dicesimulator.demo.core.dto.DiceSimulatorResponseDTO;
import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;

import java.util.List;
import java.util.Map;

public interface DiceSimulatorService {
    List<DiceSimulatorResponseDTO> simulateDiceRoll(DiceSimulatorRequestDTO requestDTO) throws Exception;
    List<SimulationResultsDTO> getSimulationDetails();
    List<DistributionPercentageResultsDTO> getDistributionPercentages(int diceCount, int diceSides);
}
