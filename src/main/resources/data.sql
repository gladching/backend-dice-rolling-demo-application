DROP TABLE IF EXISTS roll_results;
DROP TABLE IF EXISTS simulation_details;

CREATE TABLE simulation_details (
    id INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    dice_count  int NOT NULL,
    dice_sides int NOT NULL,
    roll_count int NOT NULL,
    created_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by VARCHAR(20) NOT NULL DEFAULT 'system',
    last_updated_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    last_updated_by VARCHAR(20) NOT NULL DEFAULT 'system',
    PRIMARY KEY(id));

CREATE TABLE roll_results (
    id INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    simulator_id  int NOT NULL,
    result_sum int NOT NULL,
    created_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by VARCHAR(20) NOT NULL DEFAULT 'system',
    last_updated_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    last_updated_by VARCHAR(20) NOT NULL DEFAULT 'system',
    PRIMARY KEY(id));

ALTER TABLE roll_results
ADD CONSTRAINT fk_roll_results_simulation_details FOREIGN KEY (simulator_id) REFERENCES simulation_details(id);


