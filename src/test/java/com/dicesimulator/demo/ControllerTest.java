package com.dicesimulator.demo;

import com.dicesimulator.demo.core.DiceSimulatorService;
import com.dicesimulator.demo.core.dto.DiceSimulatorRequestDTO;
import com.dicesimulator.demo.core.dto.DiceSimulatorResponseDTO;
import com.dicesimulator.demo.core.dto.DistributionPercentageResultsDTO;
import com.dicesimulator.demo.core.dto.SimulationResultsDTO;
import com.dicesimulator.demo.presentation.DiceSimulatorController;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DiceSimulatorController.class)
public class ControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private DiceSimulatorService diceSimulatorService;

    private Gson gson = new Gson();

    @Test
    public void givenValidSimulationRequest_whenPostSimulation_thenReturnResponse() throws Exception {
        DiceSimulatorResponseDTO diceSimulatorResponseDTO1 = new DiceSimulatorResponseDTO(5, 40L);
        DiceSimulatorResponseDTO diceSimulatorResponseDTO2 = new DiceSimulatorResponseDTO(5, 40L);

        List<DiceSimulatorResponseDTO> response = new ArrayList<>();
        response.add(diceSimulatorResponseDTO1);
        response.add(diceSimulatorResponseDTO2);

        DiceSimulatorRequestDTO request = validRequest();

        given(diceSimulatorService.simulateDiceRoll(validRequest())).willReturn(response);

        mvc.perform(post("/dice-simulator")
            .contentType(MediaType.APPLICATION_JSON)
            .queryParam("diceCount", new String[]{Integer.toString(request.getDiceCount())})
            .queryParam("diceSides", new String[]{Integer.toString(request.getDiceSides())})
            .queryParam("rollCount", new String[]{Integer.toString(request.getRollCount())})
        ).andExpect(status().isCreated());
    }


    @Test
    public void givenInvalidSimulationRequest_whenPostSimulation_thenReturnBadRequest() throws Exception {
        DiceSimulatorResponseDTO diceSimulatorResponseDTO1 = new DiceSimulatorResponseDTO(5, 40L);
        DiceSimulatorResponseDTO diceSimulatorResponseDTO2 = new DiceSimulatorResponseDTO(5, 40L);

        List<DiceSimulatorResponseDTO> response = new ArrayList<>();
        response.add(diceSimulatorResponseDTO1);
        response.add(diceSimulatorResponseDTO2);

        given(diceSimulatorService.simulateDiceRoll(validRequest())).willReturn(response);

        mvc.perform(post("/dice-simulator")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inValidRequest().toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnResponse_whenCalledRollDetails() throws Exception {
        List<SimulationResultsDTO> simulationResultsDTO = new ArrayList<>();

        given(diceSimulatorService.getSimulationDetails()).willReturn(simulationResultsDTO);
        mvc.perform(get("/dice-simulator/roll-details")
                .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isOk());
    }


    @Test
    public void shouldReturnResponse_whenCalledDistributionDetails() throws Exception {
        List<DistributionPercentageResultsDTO> results = new ArrayList<>();

        DiceSimulatorRequestDTO request = validRequest();
        given(diceSimulatorService.getDistributionPercentages(request.getDiceCount(), request.getDiceSides())).willReturn(results);
        mvc.perform(get("/dice-simulator/roll-details")
                .contentType(MediaType.APPLICATION_JSON)
                .queryParam("diceCount", new String[]{Integer.toString(request.getDiceCount())})
                .queryParam("diceSides", new String[]{Integer.toString(request.getDiceSides())})
                .queryParam("rollCount", new String[]{Integer.toString(request.getRollCount())})
        ).andExpect(status().isOk());
    }

    static final DiceSimulatorRequestDTO validRequest() {
        return new DiceSimulatorRequestDTO(3,6,4);
    }

    static final DiceSimulatorRequestDTO inValidRequest() {
        return new DiceSimulatorRequestDTO(300,6000,4000);
    }


}
